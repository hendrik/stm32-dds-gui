#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
import telnetlib
from PyQt4 import QtCore, QtGui

class GUI(QtGui.QWidget):
    def __init__(self):
        super(GUI, self).__init__()

        self.conn = None

        ipbox = QtGui.QHBoxLayout()
        ipconnect = QtGui.QPushButton('Connect')
        self.ipedit = QtGui.QLineEdit('172.31.10.50')
        ipbox.addWidget(self.ipedit)
        ipbox.addWidget(ipconnect)
        QtCore.QObject.connect(self.ipedit, QtCore.SIGNAL('returnPressed()'),
            self.ipConnect)
        QtCore.QObject.connect(ipconnect, QtCore.SIGNAL('clicked()'),
            self.ipConnect)

        fbox = QtGui.QHBoxLayout()
        fbox.addWidget(QtGui.QLabel('Frequency'))
        self.freq = QtGui.QDoubleSpinBox()
        self.freq.setDecimals(6)
        self.freq.setSuffix(' MHz')
        self.freq.setMaximum(400)
        self.freq.setMinimum(0)
        fbox.addWidget(self.freq)

        abox = QtGui.QHBoxLayout()
        abox.addWidget(QtGui.QLabel('Amplitude'))
        self.ampl = QtGui.QDoubleSpinBox()
        self.ampl.setMinimum(-50)
        self.ampl.setMaximum(0)
        self.ampl.setSuffix(' dBm')
        self.ampl.setValue(0)
        abox.addWidget(self.ampl)

        swbox = QtGui.QHBoxLayout()
        swbox.addWidget(QtGui.QLabel('RF-Switch'))
        self.swon = QtGui.QRadioButton("on")
        self.swon.setChecked(1)
        self.swoff = QtGui.QRadioButton("external")
        swswbox = QtGui.QHBoxLayout()
        swswbox.addWidget(self.swon)
        swswbox.addWidget(self.swoff)
        swbox.addLayout(swswbox)

        bbox = QtGui.QHBoxLayout()
        standard = QtGui.QPushButton('Set as default')
        QtCore.QObject.connect(standard, QtCore.SIGNAL('clicked()'),
            self.setStandard)
        update = QtGui.QPushButton('Update')
        QtCore.QObject.connect(update, QtCore.SIGNAL('clicked()'),
            self.update)
        bbox.addWidget(standard)
        bbox.addWidget(update)

        hbox = QtGui.QVBoxLayout()
        hbox.addStretch(1)
        hbox.addLayout(ipbox)
        hbox.addLayout(fbox)
        hbox.addLayout(abox)
        hbox.addLayout(swbox)
        hbox.addLayout(bbox)

        self.setLayout(hbox)

        self.setGeometry(300, 300, 300, 150)
        self.show()

    def ipConnect(self):
        self.conn = telnetlib.Telnet(str(self.ipedit.text()), 5024, timeout = 5)
        print 'connected to %s'%str(self.ipedit.text())

    def update(self):
        self.send(':OUTPUT:FREQ %fMHz;AMPL %fdBm\r\n'%(self.freq.value(),
                                                            self.ampl.value()))
        if self.swoff.isChecked():
            self.send(':OUTPUT off\r\n')
        else:
            self.send(':OUTPUT on\r\n')


    def setStandard(self):
        self.send(':SEQ:CLEAR;:MODE PROG\r\n')
        self.update()
        self.send(':TRIGger:SEND\r\n')
        self.send(':START:SAVE;:MODE NORMal\r\n')

    def send(self, msg):
        if not self.conn:
            print 'Failed to send: not connected'
            return
        
        print '--> %s'%msg
        self.conn.write(msg)

def main():
    app = QtGui.QApplication(sys.argv)
    gui = GUI()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
